export const openNavMobile = () => {
    const [btn, navMobileAside]= document.querySelectorAll('.nav-mobile__img--last, .nav-mobile__aside');
    btn.addEventListener('click', () => navMobileAside.classList.toggle('nav-mobile__aside--active'));
};

