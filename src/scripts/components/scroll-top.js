export const scrollTop = () => {
    const arrow = document.querySelector('.nav-mobile--arrow');

    arrow.addEventListener('click', () => {
        window.scrollTo({
            left: 0,
            top: 0
        });
    });
};