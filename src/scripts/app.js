import { openNavMobile } from "./components/nav-mobile.js";
import {scrollTop} from "./components/scroll-top.js";

openNavMobile();
scrollTop();